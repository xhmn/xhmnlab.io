---
layout: page
---
The world traumatizes you until you stop being different.

To escape this, we destroyed our identity and started over.

Now we write code and fun little vignettes about humans who shed their humanity, like this one.

Works that ~~influenced us~~ we like include:
- [Anything by]() Legion van Zechariah
- [I Sexually Identify as an Attack Helicopter](), Isabel Fall, 2020
- [Vesp: A History of Sapphic Scaphism](), Porpentine, 2016
- [17776](), Jon Bois, 2017
