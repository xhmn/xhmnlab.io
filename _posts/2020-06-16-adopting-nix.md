---
layout: post
---

We have run Linux for some time. We ran Arch for a while, then moved to Void for a bit. Recently we decided to distro hop again, to NixOS.

This made some things harder and some things easier. It took about a day to get up to speed, but then things went pretty smoothly. NixOS, being an operating system based on a package manager, has a very robust package ecosystem. We didn't have a hard time finding almost everything we needed.

## Packaging things 
We packaged several things that we were missing, including [rooster](https://github.com/conradkleinespel/rooster), our password manager. [Rust-quale](https://github.com/frewsxcv/rust-quale), one of rooster's dependencies, has a [test](https://github.com/conradkleinespel/rooster/blob/master/src/quale.rs#L62) that checks whether `/usr/bin/env` resolves to where it "should be." Nix provides `/usr/bin/env`, but like almost everything else it resolves to something inside of `/nix/store`:

````sh
xhmn@kansokusha ~> ls -l /usr/bin/env
lrwxrwxrwx 1 root root 66 Jun 17 20:39 /usr/bin/env -> /nix/store/x0jla3hpxrwz76hy9yckg1iyc9hns81k-coreutils-8.31/bin/env
````

Meaning this test fails and the build won't continue. Eventually we figured out how to get Rust to skip it:

````nix
checkFlags = ["--skip" "quale::tests::test_sh"];
````

And all was well.

We almost packaged `caps2esc`, but it comes builtin with Nix. We just had to enable a thing in `/etc/nixos/configuration.nix`:

````nix
# caps2esc
services.interception-tools.enable = true;
````

## Vidya
It being The Times, just before we executed the distro hop we bought the [Itch.io](itch.io) Bundle for Racial Justice and Equality. Unfortunately most games don't work out-of-the-box with Nix as they do on other distros, for the same reason most things don't. 
